package demo;

import stack.Stack;
import stack.StackArrayListImpl;
import stack.StackImpl;

public class StackDemo {

	public static void main(String[] args) {
		Stack<Integer> stack = new StackArrayListImpl<>();
		stack.push(4);
		stack.push(6);
		stack.push(7);
		stack.push(8);
		
		System.out.println(stack.toList());
		
		Stack<Integer> stack2 = new StackImpl<>();
		stack2.push(2);
		stack2.push(5);
		stack2.push(1);
		
		stack.addAll(stack2);
		
		System.out.println(stack.toList());
		
		Stack<Integer> stk = stack;
		
		int total = 0;
		while (!stk.empty()){
			total += stk.pop();
		}
		System.out.println(total);
	}

}